BERT Audran G1 -PASSALL
-Compilateur basé sur le langage PASCAL-
-basé sur les TPs de M.JOURLIN -

les exemples seront entourés de "<<" et ">>" et les mots clés de " "

SPECIFICITE:
	-Message d'erreur avec l'endroit
	-FOR avec TO et DOWNTO; et avec soit expression booleenne soit variable
	-Pour les entier et double possibilité de faire a:=+1
	-Switch case
	-Fonction avec 2 arguments (passage par variable)

SYNTAXE DU LANGAGE:

	Le programme fini par "." à la place du dernier ";"

	La partie déclaration des variables commence par le mot clé VAR et fini par un "."
		Types de vrariables acceptées :
		Entier : INTEGER
		Nombre à virgules : DOUBLE
		Caractère : CHAR
		Booléen : BOOLEAN
			<< 
				VAR
				a,b:INTEGER;
				z:CHAR. 
			>>

	Expression : un test qui retourne un booléen
	<<1==2>>
	Statement : instruction (peut etre une structure de controle)
	Listes des statements : IF,WHILE,FOR,DISPLAY,SWITCH,Assignement
	l'utilisation d'un ; permet de continuer le programme apres chaque ligne
	<<a:=a+2;
	a:=a+2.>>
	AssignmentStatement : déclaration de variable (nécessite un ";" à la fin)
	<<a:=a+2>>
	Structure de controle :
		IF : "IF" Expression "THEN" Statement ["ELSE" Statement]
			<<					<<
				IF (1==2) 			IF (a==2)
				THEN a:=2 			THEN a:=4
				ELSE a:=1		>>
			>>
		WHILE : "WHILE" Expression "DO" Statement
			<<
				WHILE a!=2
				DO
				a:=-1;
			>>
		FOR : "FOR" AssignementStatement "TO/DOWNTO" Expression "DO" Statement
			<<						<<							<<
				FOR a:=0 TO 10			FOR a:=0 TO a<10			FOR a:=10 DOWNTO 0
				DO						DO							DO
				DISPLAY a				DISPLAY a					DISPLAY a
			>>						>>							>>
		La variable doit etre déclaré pour le assignement statement et l'expression peut etre une variable (dans ce cas cela va jusqu a "<var") ou une expression booleenne. 
		TO increment la variable et DOWNTO la décremente
		BLOCK : "BEGIN" Statement { ";" Statement } "END"
			<<
				BEGIN
				a:=1;
				DISPLAY a;
				END
			>>
		Le ";" permet d'enchainer les instructions dans le block
		DISPLAY : DISPLAY Statement
			<<DISPLAY (1+2)>>
		SWITCH : "SWITCH" ID ":" "CASE" valeur ":" Statement "ELSE" Statement "END"
		exemple de switch :
		<<
			VAR
			a:INTEGER.
			a:=1;
			SWITCH (a) :
			CASE 1 :
			a:=a+5
			CASE 2 :
			a:=a+1
			CASE 3 :
			a:=a+10
			ELSE :
			a:=+100
			END;
			DISPLAY a.
		>>
	
	Possibilité avec les entiers et les doubles de ne pas spécifier le premier opérande si celui-ci est la variable de destination. <<a:=*2;>>
	
	Les fonctions :
		/!\ je n'ai pas eu le temps de corriger quelques problèmes :
			-on ne peut pas appeler une fonction prenant des arguments dans une fonction
			-on ne peut pas appeler une fonction avec des arguments différents dans le programme genre appeler carré avec a puis avec b ca va faire le carré de b puis le carré de b
			-> j'aurais eu plus de temps j'aurais pu les corriger j'avais une idée de comment corriger mais je n'ai plus de temps pour corriger cela
		elles prenent maximum 2 arguments et c'est un passage par variable (les variables passé en argument sont modifié dans la fonction à un assignement par exemple)
		d'abord il faut définir les fonctions avant de les appeler : "DEF" nom nombrearguments
		<<DEF c 2>>
		ensuite appel de fonction : "FUNCTION" nom ([arg,arg])
		<<FUNCTION c (a,b)>> ou <<FUNCTION b ()>>
		enfin apres le main il faut les décrire : "DEF" "FUNCTION" nom (var,var) ":"
		<<DEF FUNCTION m (i,j):>>
		un exemple d'utilisation de fonction pour calculer le carré d'une variable :
		<<
			VAR
			c:INTEGER.
			c:=10;
			DEF carre 1;
			DEF affichage 1;
			FUNCTION carre(c);
			FUNCTION affichage(c).
			DEF FUNCTION carre (i):
			i:=i*i;
			END.
			DEF FUNCTION affichage (i) :
			DISPLAY i;
			END.
		>>

EXEMPLE DE PROGRAMME PASSALL:
	Moyenne de 5 nombres :
		<<
			VAR
			a,b,c,d,e,m:INTEGER.
			a:=1;
			b:=2;
			c:=3;
			d:=4;
			e:=5;
			m:=a+b+c+d+e;
			m:=m/5;
			DISPLAY m.
		>>
	
	Table de multiplication de 6 :
	<<	
		VAR
		a,b,c:INTEGER.
		a:=6;
		b:=0;
		
		DEF add 2;
		DEF affichage 1;

		FOR c:=0 TO 10 
		DO
		BEGIN
		FUNCTION add (b,a);
		FUNCTION affichage(b);
		END.


		DEF FUNCTION add (i,j):
		i:=i+j;
		END.
		DEF FUNCTION affichage (i) :
		DISPLAY i;
		END.
	>>
	
	Carré des 10 premiers nombres entiers
	<<
		VAR
		a,b,c:INTEGER.
		a:=0;
		DEF carre 1;
		DEF affichage 1;

		FOR c:=0 TO 11
		DO
		BEGIN
		b:=a;
		FUNCTION carre(a);
		FUNCTION affichage(a);
		a:=b+1;
		END.

		DEF FUNCTION carre (i):
		i:=i*i;
		END.
		DEF FUNCTION affichage (i) :
		DISPLAY i;
		END.
	>>
