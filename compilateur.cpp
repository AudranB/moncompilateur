//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN,CHAR,DOUBLE};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum TYPES> declaredvariables;
set<string> DeclaredVariables;

map<string, int> declaredfunction;
map<string, int> nbfunction;
map<string, string> argfunction;
map<string, string>  corfunction;


string variable;
string x;

unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	bool a =corfunction.find(lexer->YYText())!=corfunction.end();
	bool b=DeclaredVariables.find(id)!=DeclaredVariables.end();
	return a+b;
}

bool IsFUNC(const char *id){
	return declaredfunction.find(id)!=declaredfunction.end();
}

void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}
void Error2(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<" : ";
	cerr<< s <<"!" <<endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"type


enum TYPES Identifier(void){
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())){
		Error("Erreur : variable non déclarée");
	}
	if ((corfunction.find(lexer->YYText())!=corfunction.end()==true) and (DeclaredVariables.find(lexer->YYText())!=DeclaredVariables.end()==false)){
		string var=corfunction[lexer->YYText()];
		type=declaredvariables[var];
		x=var;
		cout << "\tpush "<<var<<endl;
	}
	else {
		type=declaredvariables[lexer->YYText()];
		x=lexer->YYText();
		cout << "\tpush "<<lexer->YYText()<<endl;
	}
	current=(TOKEN) lexer->yylex();
	return type;
}

enum TYPES Number(void){
	bool is_a_decimal=false;
	double d;					// 64-bit float
	unsigned int *i;
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; // i points to the const double
		//cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to :
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else {
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

enum TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


enum TYPES Expression(void);			// Called by Term() and calls Term()

enum TYPES Factor(void){
	enum TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("Erreur lors de l'opération : ')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else if (current==ADDOP or current==MULOP){
		type=declaredvariables[variable];
	}
	else if (current==NUMBER){
		type=Number();
	}
	else if (current==ID){
		type=Identifier();
	}
	else if (current==CHARCONST){
		type=CharConst();
	}
	else{
		Error("Erreur lors de l'opération : '(', chiffre ou caractère attendus");
	}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	enum TYPES type;
	enum TYPES type2;
	OPMUL mulop;
	type=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if (type2!=type){
			Error2("Erreur lors de l'opération : types différents");
		}
		switch(mulop){
			case AND:
				if(type!=BOOLEAN){
					Error("Erreur lors de l'opération : BOOLEAN attendu pour l'opérateur AND");
				}
				cout << "\tpop %rbx"<<endl;
				cout << "\tpop %rax"<<endl;
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if (type!=INTEGER && type!=DOUBLE){
					Error("Erreur lors de l'opération : nombre attendu pour la multiplication");
				}
				if (type==INTEGER){
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else {
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;

				}
				break;
			case DIV:
				if (type!=INTEGER && type!=DOUBLE){
					Error("Erreur lors de l'opération : nombre attendu pour la division");
				}
				if (type==INTEGER){
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else {
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				}
				break;
			case MOD:
				if (type!=INTEGER){
					Error("Erreur lors de l'opération : entier attendu pour le modulo");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("Erreur lors de l'opération : opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	enum TYPES type;
	enum TYPES type2;
	OPADD adop;
	type=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if (type2!=type){
			Error2("Erreur lors de l'opération : mauvais type");}
		switch(adop){
			case OR:
				if(type!=BOOLEAN){
					Error2("Erreur lors de l'opération : BOOLEAN attendu pour l'opérateur OR");
				}
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;
			case ADD:
				if (type!=INTEGER && type!=DOUBLE){
					Error2("Erreur lors de l'opération : nombre attendu pour une addition");
				}
				if (type==INTEGER){
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				cout << "\tpush %rax"<<endl;			// store result
				}
				else {
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				}
				break;
			case SUB:
				if (type!=INTEGER && type!=DOUBLE){
					Error2("Erreur lors de l'opération : nombre attendu pour une soustraction");
				}
				if (type==INTEGER){
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				}
				break;
			default:
				Error("Erreur lors de l'opération : opérateur additif inconnu");
		}
	}
	return type;

}

void VarDeclaration(void){
	vector<string> v;
	if(current!=ID)
		Error("Erreur lors de la déclaration des variables : un identificater était attendu");
	/*
	if (ct==1){
		current=(TOKEN) lexer->yylex();
	}*/
	DeclaredVariables.insert(lexer->YYText());			//VARIABLE SET insertion
	declaredvariables[lexer->YYText()]=INTEGER;
	v.push_back(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Erreur lors de la déclaration des variables : un identificateur était attendu");
		DeclaredVariables.insert(lexer->YYText());
		declaredvariables[lexer->YYText()]=INTEGER;
		v.push_back(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if (current!=COLON) {
		Error("Erreur lors de la déclaration des variables : ':' attendu");
	}
	else {
		current=(TOKEN) lexer->yylex();
		if ((strcmp(lexer->YYText(),"INTEGER")==0)){
			for (int i=0;i<v.size();i++){
				declaredvariables[v[i]]=INTEGER;
				cout << v[i] << ":\t.quad 0"<<endl;
			}
		}
		else if ((strcmp(lexer->YYText(),"BOOLEAN")==0)){
			for (int i=0;i<v.size();i++){
				declaredvariables[v[i]]=BOOLEAN;
				cout << v[i] << ":\t.quad 0"<<endl;
			}
		}
		else if ((strcmp(lexer->YYText(),"DOUBLE")==0)){
			for (int i=0;i<v.size();i++){
				declaredvariables[v[i]]=DOUBLE;
				cout << v[i] << ":\t.double 0.0"<<endl;
			}
		}
		else if ((strcmp(lexer->YYText(),"CHAR")==0)){
			for (int i=0;i<v.size();i++){
				declaredvariables[v[i]]=CHAR;
				cout << v[i] << ":\t.byte 0"<<endl;
			}
		}
		else {
			Error("Erreur lors de la déclaration des variables : type de variable inconnu");
		}
		current=(TOKEN) lexer->yylex();
	}
}

// DeclarationPart :=yes "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if((strcmp(lexer->YYText(),"VAR")!=0))
		Error("Erreur lors de la déclaration des variables : VAR attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	cout<<"FormatString1:\t   .string \"%llu\\n\"\t   # used by printf to display 64-bit unsigned integers"<<endl;
	cout <<"FormatString2:\t.string \"%lf\\n\"\t# used by printf to display 64-bit floating point numbers"<<endl;
	cout << "FormatString3:\t.string \"%c\\n\"\t# used by printf to display a 8-bit single character"<<endl;
	cout << "TrueString:\t.string \"TRUE\\n\"\t# used by printf to display the boolean value TRUE"<<endl;
	cout << "FalseString:\t.string \"FALSE\\n\"\t# used by printf to display the boolean value FALSE"<<endl;
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("Erreur lors de la déclaration des variables : caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(){
	enum TYPES type;
	enum TYPES type2;
	OPREL oprel;
	type=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if (type2!=type){
			Error2("Erreur dans l'expression : mauvais type");
		}
		if (type==INTEGER){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else if (type==DOUBLE){
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu (Expression)");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;
		cout << "Suite"<<TagNumber<<":"<<endl;
		type=BOOLEAN;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
 void AssignementStatement(){
	enum TYPES type;
	enum TYPES type2;
	if(current!=ID)
		Error("Erreur dans l'assignement : Identificateur attendu");
	if(!IsDeclared(lexer->YYText()) and (corfunction.find(lexer->YYText())!=corfunction.end())){
		Error("Erreur dans l'assignement : variable non déclarée");
	}
	variable=lexer->YYText();
	type=declaredvariables[variable];
	if ( corfunction.find(lexer->YYText()) != corfunction.end()){
		variable=corfunction[lexer->YYText()];
		type=declaredvariables[variable];
	}
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("Erreur dans l'assignement : caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if (current==ADDOP or current==MULOP){
		if (type!=INTEGER and type!=DOUBLE){
			Error2("Erreur dans l'assignement : Integer ou Double attendu pour ce genre d'opération");
		}
		cout << "\tpush "<<variable<<endl;
		type2=Expression();
	}
	else {
		type2=Expression();
	}
	if(type2!=type){
		Error2("Erreur dans l'assignement : variable et expression de type different");
	}
	if(type==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else {
		cout << "\tpop "<<variable<<endl;
	}
}

void DisplayStatement(void){
	int tag=++TagNumber;
	cout<<"DISPLAY"<<tag<<" :"<<endl;
	if(strcmp(lexer->YYText(),"DISPLAY")!=0)
		Error("Erreur dans le DISPLAY : DISPLAY attendu");
	current=(TOKEN) lexer->yylex();
	TYPES type=Expression();
	if (type==INTEGER){
		cout<<"\tpop %rdx                     # The value to be displayed"<<endl;
		cout<<"\tmovq $FormatString1, %rsi    # \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	__printf_chk@PLT"<<endl;
	}
	else if(type==BOOLEAN){
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rsi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rsi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tmovl	$1, %edi"<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	__printf_chk@PLT"<<endl;
	}
	else if (type==DOUBLE){
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
	else if (type==CHAR){
		cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;

	}
	else {
		Error("Erreur dans le DISPLAY : mauvais type données");
	}
	//cout << "\tmovl	$1, %edi"<<endl;
	//cout << "\tmovl	$0, %eax"<<endl;
	//cout << "\tcall	__printf_chk@PLT"<<endl;

}

void Statement(void);

void SwitchStatement(void){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"SWITCH")!=0)
		Error("Erreur dans le SWITCH : SWITCH attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"SWITCH"<<tagNum<<" :"<<endl;
	TYPES type=Expression();
	string varsw=x;
	if(type==BOOLEAN or type==DOUBLE){
		Error2("Erreur dans le SWITCH : Type différent de bool et double attendu dans switch");
	}
	if (current!=COLON){
		Error("Erreur dans le SWITCH : ':' attendu");
	}
	current=(TOKEN) lexer->yylex();
	int casenum=0;
	cout<<"\tpop %rcx"<<endl;
	while (strcmp(lexer->YYText(),"CASE")==0){
		cout<<"CASE"<<tagNum<<casenum<<" :"<<endl;
		current=(TOKEN) lexer->yylex();
		TYPES type2=Expression();
		if(type2!=type){
			Error2("Erreur dans le SWITCH : Type différent");
		}
		if (current!=COLON){
			Error("Erreur dans le SWITCH : ':' attendu");
		}
		current=(TOKEN) lexer->yylex();
		cout<<"\tmovq %rcx, %rax"<<endl;
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tcmpq %rax,%rbx"<<endl;
		cout<<"\tjne CASE"<<tagNum<<casenum+1<<endl;
		Statement(); 
		casenum++;
		cout<<"\tjmp FINSwitch"<<tagNum<<endl;
	}
	if (strcmp(lexer->YYText(),"ELSE")!=0){
		Error("Erreur dans le SWITCH : Else attendu");
	}
	current=(TOKEN) lexer->yylex();
	if (current!=COLON){
		Error("Erreur dans le SWITCH : ':' attendu");
	}
	cout<<"CASE"<<tagNum<<casenum<<" :"<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	if (strcmp(lexer->YYText(),"END")!=0){
		Error("Erreur dans le SWITCH : END attendu");
	}
	cout<<"FINSwitch"<<tagNum<<" :"<<endl;
	current=(TOKEN) lexer->yylex();
}

void IFStatement(void){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"IF")!=0)
		Error("Erreur dans le IF : IF attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"IF"<<tagNum<<" :"<<endl;
	TYPES type=Expression();
	if(type!=BOOLEAN){
		Error("Erreur dans le IF : BOOLEAN attendu dans expression du IF");
	}
	cout<<"\tpop %rax"<<endl;
	cout<<"\tmovq $0,%rbx"<<endl;
	cout<<"\tcmpq %rax,%rbx"<<endl;
	cout<<"\tje ELSE"<<tagNum<<endl;
	//current=(TOKEN) lexer->yylex();
	if (strcmp(lexer->YYText(),"THEN")!=0){
		Error("Erreur dans le IF : THEN attendu");
	}
	cout<<"THEN"<<tagNum<<" : "<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp END"<<tagNum<<endl;
	//current=(TOKEN) lexer->yylex();
	cout<<"ELSE"<<tagNum<<" : "<<endl;
	if (strcmp(lexer->YYText(),"ELSE")==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"END"<<tagNum<<" :"<<endl;
}

void WHILEStatement(void){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"WHILE")!=0)
		Error("Erreur dans le WHILE : WHILE attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"WHILE"<<tagNum<<" :"<<endl;
	TYPES type=Expression();
	if(type!=BOOLEAN){
		Error("Erreur dans le WHILE : Bool attendu dans expression du while");
	}
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmpq $0,%rax"<<endl;
	cout<<"\tje WHILEFin"<<tagNum<<endl;
	//current=(TOKEN) lexer->yylex();
	if (strcmp(lexer->YYText(),"DO")!=0){
		Error("Erreur dans le WHILE : DO attendu");
	}
	cout<<"DO"<<tagNum<<" : "<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp  WHILE"<<tagNum<<endl;
	cout<<"WHILEFin"<<tagNum<<" :"<<endl;
}

void FORStatement(void){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"FOR")!=0)
		Error("Erreur dans le FOR : FOR attendu");
	cout<<"FOR"<<tagNum<<" :"<<endl;
	current=(TOKEN) lexer->yylex();
	AssignementStatement();
	string var=variable;
	//current=(TOKEN) lexer->yylex();
	bool x=false;
	if (strcmp(lexer->YYText(),"TO")!=0 and strcmp(lexer->YYText(),"DOWNTO")!=0){
		Error("Erreur dans le FOR : TO ou DOWNTO attendu");
	}
	if (strcmp(lexer->YYText(),"DOWNTO")==0){
		x=true;
	}
	cout<<"TO"<<tagNum<<" : "<<endl;
	current=(TOKEN) lexer->yylex();
	enum TYPES type=Expression();
	if (type!=BOOLEAN and type!=INTEGER){
		Error("Erreur dans le FOR : Booleen ou entier attendu");
	}
	cout<<"\tpop %rbx"<<endl;
	if (type==BOOLEAN){
		cout<<"\tcmpq $0,%rbx"<<endl;
	}
	else {
		cout<<"\tmovq "<<var<<",%rax"<<endl;
		cout<<"\tcmpq %rbx,%rax"<<endl;
	}
	cout<<"\tje FORFin"<<tagNum<<endl;
	//current=(TOKEN) lexer->yylex();
	if (strcmp(lexer->YYText(),"DO")!=0){
		Error("Erreur dans le FOR : DO attendu");
	}
	cout<<"DO"<<tagNum<<" : "<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tmovq "<<var<<",%rax"<<endl;
	if (x==true){
		cout<<"\tsubq $1,%rax"<<endl;
	}
	else {
		cout<<"\taddq $1,%rax"<<endl;
	}
	cout<<"\tpush %rax"<<endl;
	cout<<"\tpop "<<var<<""<<endl;
	//cout<<"\tmovq %rax,$"<<var<<endl;
	cout<<"\tjmp  TO"<<tagNum<<endl;
	cout<<"FORFin"<<tagNum<<" :"<<endl;
}



void BLOCKStatement(){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"BEGIN")!=0){
		Error("BEGIN attendu (BLOCKStatement)");
	}
	cout<<"BEGIN"<<tagNum<<" : "<<endl;
	current=(TOKEN) lexer->yylex();
	while (strcmp(lexer->YYText(),"END")!=0){
		Statement();
		if (current!=SEMICOLON ){
			Error("';' attendu (BLOCKStatement)");
		}
		current=(TOKEN) lexer->yylex();
	}
	current=(TOKEN) lexer->yylex();
	cout<<"END"<<tagNum<<" : "<<endl;
	
}
void DEFStatement(){
	while (strcmp(lexer->YYText(),"DEF")==0){
		current=(TOKEN) lexer->yylex();
		if(strcmp(lexer->YYText(),"FUNCTION")!=0){
			Error("Erreur dans la fonction : FUNCTION attendu");
		}
		current=(TOKEN) lexer->yylex();
		string var=lexer->YYText();
		int nbarg=0;
		unsigned long tagNum=declaredfunction[var];
		cout<<"DEF"<<tagNum<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		if (current!=RPARENT ){
			Error("Erreur dans la fonction : '(' attendu");
		}
		current=(TOKEN) lexer->yylex();
		if (current!=LPARENT){
			//recup arg1
			if (current!=ID){
				Error("Erreur dans la fonction : ID attendu");
			}
			string arg1=lexer->YYText();
			corfunction[arg1]=argfunction["0"];
			//cout<<"! "<<arg1<<" : " <<argfunction["0"]<<" ; "<<corfunction[arg1]<<endl;
			current=(TOKEN) lexer->yylex();
			nbarg=1;
			if (current!=LPARENT and current!=COMMA){
				Error("Erreur dans la fonction : ')' ou ',' attendu");
			}
			if (current==COMMA){
				current=(TOKEN) lexer->yylex();
				//recup arg2
				if (current!=ID){
					Error("Erreur dans la fonction : ID attendu");
				}
				string arg2=lexer->YYText();
				corfunction[arg2]=argfunction["1"];
				nbarg=2;
				current=(TOKEN) lexer->yylex();
				if (current!=LPARENT){
					Error("Erreur dans la fonction : ')'  attendu");
				}
			}
		}
		nbfunction[var]=nbarg;
		current=(TOKEN) lexer->yylex();
		if (current!=COLON ){
			Error("Erreur dans la fonction : ':' attendu");
		}
		current=(TOKEN) lexer->yylex();
		do {
			Statement();
			if (current!=SEMICOLON ){
				Error("Erreur dans la fonction : ';' attendun");
			}
			current=(TOKEN) lexer->yylex();
		} while (strcmp(lexer->YYText(),"END")!=0);
		current=(TOKEN) lexer->yylex();
		cout<<"\tret"<<endl;
		if (current!=DOT ){
			Error("Erreur dans la fonction : '.' attendu");
		}
		current=(TOKEN) lexer->yylex();
		/*
		if (nbarg>=1){
			declaredvariables.erase(arg1);
			DeclaredVariables.erase(arg1);
			if (nbarg==2){
				declaredvariables.erase(arg2);
				DeclaredVariables.erase(arg2);
			}
		} */
	}
	
}

void DEF(){
	unsigned long tagNum=++TagNumber;
	if(strcmp(lexer->YYText(),"DEF")!=0){
		Error("Erreur lors de la déclaration de la fonction : DEF attendu");
	}
	current=(TOKEN) lexer->yylex();
	if (current!=ID){
		Error("Erreur lors de la déclaration de la fonction : nom de fonction attendu");
	}
	string nom=lexer->YYText();
	declaredfunction[nom]=tagNum;
	current=(TOKEN) lexer->yylex();
	int nb=atoi(lexer->YYText());
	if (current!=NUMBER or (nb>2)){
		Error("Erreur lors de la déclaration de la fonction : nombre attendu et le nombre<=2");
	}
	nbfunction[nom]=nb;
	current=(TOKEN) lexer->yylex();
}

void FUNCTIONStatement(){
		unsigned long tagNum=++TagNumber;
		if(strcmp(lexer->YYText(),"FUNCTION")!=0){
			Error("Erreur lors de l'appel de la fonction : FUNCTION attendu");
		}
		cout<<"FCT"<<tagNum<<" :"<<endl;
		current=(TOKEN) lexer->yylex();
		string fc=lexer->YYText();
		bool is=IsFUNC(lexer->YYText());
		if (is==false){
			Error("Erreur lors de l'appel de la fonction : fonction non déclarée ");
		}
		cout<<"\tcall "<<"DEF"<<declaredfunction[fc]<<endl;
		int nbarg=nbfunction[fc];
		current=(TOKEN) lexer->yylex();
		if (current!=RPARENT ){
			Error("Erreur lors de l'appel de la fonction : '(' attendu");
		}
		int nbarg1=0;
		current=(TOKEN) lexer->yylex();
		if (current!=LPARENT){
			//recup arg1
			argfunction["0"]=lexer->YYText();
			//cout <<"\tpush "<<lexer->YYText()<<endl;
			nbarg1=1;
			current=(TOKEN) lexer->yylex();
			if (current!=LPARENT and current!=COMMA){
				Error("Erreur lors de l'appel de la fonction : ')' ou ',' attendu");
			}
			if (current==COMMA){
				current=(TOKEN) lexer->yylex();
				//recup arg2
				argfunction["1"]=lexer->YYText();
				//cout <<"\tpush "<<lexer->YYText()<<endl;
				nbarg1=2;
				current=(TOKEN) lexer->yylex();
				if (current!=LPARENT){
					Error("Erreur lors de l'appel de la fonction : ')' attendu");
				}
			}
		}
		if (nbarg!=nbarg1){
			Error("Erreur lors de l'appel de la fonction : Nombre d'argument pas cohérent avec la déclaration");
		}
		current=(TOKEN) lexer->yylex();
		cout<<"ENDFCT"<<tagNum<<" :"<<endl;
}

// Statement := AssignementStatement
void Statement(void){
	if (current==STATEK){
		if (strcmp(lexer->YYText(),"IF")==0){
			IFStatement();
		}
		else if ((strcmp(lexer->YYText(),"FOR")==0)){
			FORStatement();
		}
		else if ((strcmp(lexer->YYText(),"WHILE")==0)){
			WHILEStatement();
		}
		else if ((strcmp(lexer->YYText(),"BEGIN")==0)){
			BLOCKStatement();
		}
		else if ((strcmp(lexer->YYText(),"DISPLAY")==0)){
			DisplayStatement();
		}
		else if ((strcmp(lexer->YYText(),"SWITCH")==0)){
			SwitchStatement();
		}
		else if ((strcmp(lexer->YYText(),"FUNCTION")==0)){
			FUNCTIONStatement();
		}
		else if ((strcmp(lexer->YYText(),"DEF")==0)){
			DEF();
		}
		else {
			Error("Mot clé inconnu (Statement)");
		}
	}
	else {
		if (current==ID){
			AssignementStatement();
		}
		else {
			Error("Instruction attendu (Statement)");
		}
	}

}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("Caractère '.' attendu (StatementPart)");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if((strcmp(lexer->YYText(),"VAR")==0))
		DeclarationPart();
	StatementPart();
	if ((strcmp(lexer->YYText(),"DEF")==0)){
		cout<<"\tjmp FIN"<<endl;
		DEFStatement();
	}
	cout<<"FIN :"<<endl;
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
