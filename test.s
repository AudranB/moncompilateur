			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:	   .string "%llu\n"	   # used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf\n"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c\n"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE\n"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE\n"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
c:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $2
	pop a
	push $3
	pop b
	push $4
	pop c
FCT3 :
	call DEF1
ENDFCT3 :
FCT4 :
	call DEF2
ENDFCT4 :
FCT5 :
	call DEF1
ENDFCT5 :
FCT6 :
	call DEF2
ENDFCT6 :
	jmp FIN
DEF1 : 
	push b
	push b
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop b
	ret
DEF2 : 
DISPLAY7 :
	push b
	pop %rdx                     # The value to be displayed
	movq $FormatString1, %rsi    # "%llu\n"
	movl	$0, %eax
	call	__printf_chk@PLT
	ret
FIN :
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
